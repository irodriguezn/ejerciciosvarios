/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Peaje.modelo;

import java.util.LinkedList;

/**
 *
 * @author nacho
 */
public class Cola {
    String nombre;
    LinkedList<Coche> coches=new LinkedList();

    public Cola(String nombre) {
        this.nombre = nombre;
    }
    
    public void entrarCola(Coche c) {
        coches.add(0, c);
    }
    
    public Coche salirCola() {
        Coche c=null;
        coches.getLast();
        coches.removeLast();
        return c;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Cola{" + "nombre=" + nombre + ", coches=" + coches + '}';
    }
}
