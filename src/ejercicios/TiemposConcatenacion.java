/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

/**
 *
 * @author nacho
 */
public class TiemposConcatenacion {
    public static void main(String[] args) {
        long inicio, fin;
        //final int tiempo = 10000;
        final int tiempo = 10000000;
        
        if (tiempo<=10000) {
            String s = new String();
             inicio = System.currentTimeMillis();
            for (int i = 0; i < tiempo; i++) {
              s = s + "mitexto";
            }
            fin = System.currentTimeMillis();
            System.out.println("Tiempo del String: " + (fin - inicio));
            System.out.println(s);
        }
        
        StringBuffer sbuffer = new StringBuffer();
        inicio = System.currentTimeMillis();
        for (int i = 0; i < tiempo; i++) {
          sbuffer.append("mitexto");
        }
        sbuffer.toString();
        fin = System.currentTimeMillis();
        System.out.println("Tiempo del StringBuffer: " + (fin - inicio));

        StringBuilder sbuilder = new StringBuilder();
        inicio = System.currentTimeMillis();
        for (int i = 0; i < tiempo; i++) {
          sbuilder.append("mitexto");
        }
        sbuilder.toString();
        fin = System.currentTimeMillis();
        System.out.println("Tiempo del StringBuilder: " + (fin - inicio));
    }
}
