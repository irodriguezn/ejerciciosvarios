/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Peaje.modelo;

/**
 *
 * @author nacho
 */
public class Autopista {
    private Cola EFECTIVO;
    private Cola IMPORTE_EXACTO;
    private Cola TARJETA;

    public Autopista() {
        this.EFECTIVO = new Cola("EFECTIVO");
        this.IMPORTE_EXACTO = new Cola("IMPORTE_EXACTO");
        this.TARJETA = new Cola("TARJETA");
    }
    
    public void ponerCola(int cola, Coche c) {
        switch (cola) {
            case 1:
                EFECTIVO.entrarCola(c);
                break;
            case 2: 
                IMPORTE_EXACTO.entrarCola(c);
                break;
            case 3:
                TARJETA.entrarCola(c);
                break;
        }
    }
    
    public Coche sacarCola (int cola) {
        Coche c=null;
        switch (cola) {
            case 1:
                c=EFECTIVO.salirCola();
                break;
            case 2: 
                c=IMPORTE_EXACTO.salirCola();
                break;
            case 3:
                c=TARJETA.salirCola();
                break;
        }
        return c;
    }

    @Override
    public String toString() {
        return "EFECTIVO=" + EFECTIVO + "\n\n IMPORTE_EXACTO=" + IMPORTE_EXACTO + "\n\n TARJETA=" + TARJETA;
    }
    
}
