/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author nacho
 */
public class ExpRegulares {

    /**
     * @param args the command line arguments
     */
    
    static final String patronEmail="^[\\w.-]+@[\\w.-]+\\.[a-zA-Z]{2,6}$";
    static final String patronDni="^[0-9]{8}[A-Z]$";
    static final String patronNif="^[KLM][0-9]{7}[A-Z]$";
    static final String patronNie="^[XYZ][0-9]{7}[0-9]$";
    static final String patronCif="[ABCDEFGHJNPQRSUVW][0-9]{7}[[A-Z],[0-9]]$";
    
    public static void main(String[] args) {
        Pattern p = Pattern.compile(patronEmail);
        Matcher m = p.matcher("ahola.z@gmail.com");
        System.out.println("email 1: " + m.matches());
         m = p.matcher("aho.la.z@g.mail.com");
        System.out.println("email 2: " + m.matches());
        p=Pattern.compile(patronCif);
        m=p.matcher("A1234567E");
        System.out.println("cif 1: " + m.matches());
        m=p.matcher("F12345679");
        System.out.println("cif 2: " + m.matches());
    }
    
}
