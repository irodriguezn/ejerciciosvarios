/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Peaje.controlador;

import Peaje.modelo.*;
import Peaje.vista.*;

/**
 *
 * @author nachorod
 */
public class Controlador {
    Autopista autopista;
    Vista vista;
    VistaAutopista vistaAutopista;
    
    public Controlador(Autopista autopista, Vista vista) {
        this.autopista=autopista;
        this.vista=vista;
        Coche c1;
        int cola;
        
        for (int i=1; i<=5; i++) {
            c1=new Coche("matricula"+i, "modelo"+i, "color"+i);            
            cola=(int)(Math.random()*3+1);
            autopista.ponerCola(cola, c1);
        }
        
        VistaAutopista va=new VistaAutopista();
        va.mostrar(autopista);
        
        cola=(int)(Math.random()*3+1);
        c1=autopista.sacarCola(cola);
        VistaCoche vc=new VistaCoche();
        if (c1!=null) {
            vc.mostrarCoche(c1);
        }
        
        va.mostrar(autopista);
        
    }
    
}
