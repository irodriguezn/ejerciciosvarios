/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author nacho
 */
public class Web {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://cara.hol.es/beatles/");
            URLConnection urlCon = url.openConnection();
            // acceso al contenido web
            InputStream is = urlCon.getInputStream();

            // Fichero en el que queremos guardar el contenido
            FileOutputStream fos = new FileOutputStream("./src/web/beatles/index.html");
            //BufferedOutputStream b=new BufferedOutputStream(fos);
            //BufferedInputStream bi=new BufferedInputStream(is);
            // buffer para ir leyendo.
            byte [] array = new byte[1000];
            // Primera lectura y bucle hasta el final
            int leido = is.read(array);
            //leido=bi.read();
            while (leido > 0) {
                fos.write(array,0,leido);
                leido=is.read(array);
            }

            // Cierre de conexion y fichero.
            is.close();
            fos.close();
            
        } catch (Exception e) {

        }   
    }
}
